var app = angular.module('hrCloudAssignment', ['ngMessages']);
/**
 * Directive user for phone number validation
 */
app.directive('phoneNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, mCtrl) {
            function phoneNumberValidation(value) {
                if ((angular.isNumber(+value) && !isNaN(value)) && value > 0) {
                    mCtrl.$setValidity('phone-number', true);
                } else {
                    mCtrl.$setValidity('phone-number', false);
                }
                return value;
            }
            mCtrl.$parsers.push(phoneNumberValidation);
        }
    }
});

app.controller('contactsController', function ($scope, $http, $window, $location) {

    // var url = 'http://localhost:49489/api/Contacts';
    var url = 'http://puzjakcontactlist.azurewebsites.net/api/Contacts';

    /**
     * Used for disabling view elements when needed
     */
    $scope.isClientBusy = true;

    /**
     * Gets all contacts from the server and stores them in "allContacts" model.
     * If response is successfull and there are no contacts or if request failed, 
     * user will be alerted.
     * If URL contains id of a contact, details modal will be shown.
     */

    $http.get(url).then(function successCallBack(response) {
        if (response.data.length !== 0) {
            $scope.allContacts = response.data;
            $scope.alert.isVisible = false;
            var path = $location.absUrl();
            if (!(path.endsWith('index.html') ||
                path.endsWith('#!') ||
                path.endsWith('#!/'))) {
                var id = path.slice(path.lastIndexOf('/') + 1);
                $scope.contactDetails(id);
            }
        } else {
            updateAlert("alert-warning", "There are no contacts, please add a new one");
        }
        $scope.isClientBusy = false;
    }, function errorCallBack(response) {
        if (response.status == -1) {
            updateAlert("alert-warning", "Couldn't establish connection with server. Please try again later.");
        } else {
            updateAlert("alert-danger", "Error: " + response.status + " " + response.statusText);
        }
        $scope.isClientBusy = false;
    });

    /**
     * Alert which is used to show client state. It has message which will 
     * be displayed, class which represents bootstrap class (e.g. "alert-info") 
     * and visibility parameter
     */
    $scope.alert = {
        message: "Please wait until contacts load...",
        class: "alert-info",
        isVisible: true
    };


    $scope.hideAlert = function () {
        $scope.alert.isVisible = false;
    }

    /**
     * Updates alert with given status and message. Status must be
     * bootstrap class represented as string (e.g. "alert-info")
     * @param {string} status 
     * @param {string} message
     */
    function updateAlert(status, message) {
        $scope.alert.isVisible = false;
        $scope.alert.class = status;
        $scope.alert.message = message;
        $scope.alert.isVisible = true;
    }

    /**
     * Contact model which is used to display existing contact details
     * or which is used as entry when adding new contact.
     */
    $scope.temporaryContact = {
        id: "00000000-0000-0000-0000-000000000000",
        firstName: "",
        lastName: "",
        nickname: "",
        address: "",
        phoneNumbers: [],
        emails: [],
        tags: [],
        isBookmarked: false
    };

    function resetTmpContact() {
        $scope.temporaryContact.id = "00000000-0000-0000-0000-000000000000";
        $scope.temporaryContact.firstName = "";
        $scope.temporaryContact.lastName = "";
        $scope.temporaryContact.nickname = "";
        $scope.temporaryContact.address = "";
        $scope.temporaryContact.phoneNumbers = [];
        $scope.temporaryContact.emails = [];
        $scope.temporaryContact.tags = [];
        $scope.temporaryContact.isBookmarked = false;
    };

    /**
     * Variable which determins if temporary contact will be empty and will 
     * serve for adding new contact or if it will display existing contact details.
     * It is also used to show/hide certain functions in contactModal
     */
    $scope.isContactNew = false;

    /**
     * Function which is called when 'Add new contact' button is pressed.
     * It opens contactModal and prepares it for use.
     */
    $scope.addNewContactButton = function () {
        $http.get(url + "/Tags").then(function successCallBack(response) {
            $scope.tags = response.data;
        });
        resetTmpContact();
        $scope.isContactNew = true;
        $('#contactModal').modal('show');
    }

    /**
     * Function which sends POST request to add new contact. 
     * While client is waiting for response "isClientBusy" flag is true;
     * If response is successfull, new contact will be added to "allContacts"
     * and user will be notified. 
     * If it failed user will also be notified.
     */
    $scope.addNewContact = function () {
        $scope.isClientBusy = true;
        updateAlert("alert-info", "Please wait...");
        $http.post(url, angular.copy($scope.temporaryContact), {
            headers: { 'Content-Type': 'application/json' }
        }).then(function successCallBack(response) {
            $scope.allContacts.push(response.data);
            updateAlert("alert-success", "Successfully added " + response.data.firstName + " " + response.data.lastName);
            $scope.isClientBusy = false;
        }, function errorCallBack(response) {
            if (response.status == -1) {
                updateAlert("alert-warning", "Couldn't establish connection with server. Please try again later.");
            } else {
                updateAlert("alert-danger", "Error: " + response.status + " " + response.statusText);
            }
            $scope.isClientBusy = false;
        });
        resetTmpContact();
        $('#contactModal').modal('hide');
    };



    /**
     * When called it prepares "temporaryContact" model to show selected contact details
     * in "contactModal".
     * It also makes sure that "searchModal" is closed in case that details button
     * is pressed from that modal.
     * After it gets contact details, it will send request to get all tags which 
     * will be used to display which tags does contact has.
     * If contact is not found, "contactModal" will be closed and user will be notified.
     */
    $scope.contactDetails = function (id) {
        updateAlert("alert-info", "Please wait...");
        $scope.isContactNew = false;
        var contact = null;
        $http.get(url + '/' + id).then(function successCallBack(response) {
            $scope.hideAlert();
            if (response.status !== 204) {
                contact = response.data;
                $scope.temporaryContact = angular.copy(contact);
                $location.url(contact.id);
                $('#searchModal').modal('hide');
                $('#contactModal').modal('show');
            } else {
                updateAlert("alert-warning", "This contact doesn't exist");
            }
        }, function errorCallBack(response) {
            $('#contactModal').modal('hide');
            $location.url('');
            if (response.status == -1) {
                updateAlert("alert-warning", "Couldn't establish connection with server. Please try again later.");
            } else {
                updateAlert("alert-danger", "Error: " + response.status + " " + response.statusText);
            }
        }).then(function getTags() {
            $http.get(url + "/Tags").then(function successCallBack(response) {
                $scope.tags = response.data;
            });
        })
    };

    /**
     * Functions which are used when user adds aditional or removes unwanted 
     * numbers/emails while adding new contact or watching contact details  
     */
    $scope.addNewNumber = function () {
        $scope.temporaryContact.phoneNumbers.push({ 'id': '00000000-0000-0000-0000-000000000000', 'number': '' });
    }

    $scope.deleteNumber = function (index) {
        $scope.temporaryContact.phoneNumbers.splice(index, 1);
    }
    $scope.addNewEmail = function () {
        $scope.temporaryContact.emails.push({ 'id': '00000000-0000-0000-0000-000000000000', 'address': '' });
    }

    $scope.deleteEmail = function (index) {
        $scope.temporaryContact.emails.splice(index, 1);
    }


    /**
     * Function which is called when "Save changes" button is pressed
     * while watching contact details. It sends out PUT request to server 
     * with edited contact as data. If response is successfull, edited contact
     * is returned, "allContacts" model is updated and user is notified. 
     * User is also notified if request fails.
     * Function also changes "isClientBusy" flag until response arrives and
     * closes "contactModal"
     */
    $scope.saveChanges = function () {
        $scope.isClientBusy = true;
        updateAlert("alert-info", "Please wait...");
        $http.put(url, angular.copy($scope.temporaryContact), {
            headers: { 'Content-Type': 'application/json' }
        }).then(function successCallBack(response) {
            for (var i = 0; i < $scope.allContacts.length; i++)
                if ($scope.allContacts[i].id == response.data.id) break;
            $scope.allContacts.splice(i, 1);
            $scope.allContacts.push(response.data);
            updateAlert("alert-success", "Successfully updated " + response.data.firstName + " " + response.data.lastName);
            $scope.isClientBusy = false;
        }, function errorCallBack(response) {
            if (response.status == -1) {
                updateAlert("alert-warning", "Couldn't establish connection with server. Please try again later.");
            } else {
                updateAlert("alert-danger", "Error: " + response.status + " " + response.statusText);
            }
            $scope.isClientBusy = false;
        });
        resetTmpContact();
        $scope.tags = [];
        $('#contactModal').modal('hide');
        $location.path('');
    }

    /**
     * Function which is called every time "contactModal" is closed with
     * "cancel" button
     */
    $scope.discardChanges = function () {
        if (!$location.absUrl().endsWith('index.html')) {
            $location.path('');
        }
        resetTmpContact();
        $scope.tags = [];
        $('#contactModal').modal('hide');
    }

    /**
     * Used to send out DELETE request to server after user confirmation.
     * Function changes "isClientBusy" flag until response arrives and user
     * is alerted if the response is successfull or failed.
     * After successfull response "allContacts" model is updated.
     */
    $scope.deleteContact = function () {

        var alert = $window.confirm("Are you sure you want to delete this contact " + $scope.temporaryContact.firstName + " " + $scope.temporaryContact.lastName + "?");

        if (alert) {
            $scope.isClientBusy = true;
            updateAlert("alert-info", "Please wait...");
            var contact = angular.copy($scope.temporaryContact);
            $http.delete(url + "/" + angular.copy($scope.temporaryContact.id))
                .then(function successCallBack(response) {
                    for (var i = 0; i < $scope.allContacts.length; i++)
                        if ($scope.allContacts[i].id == contact.id) break;
                    updateAlert("alert-success", "Successfully deleted " + contact.firstName + " " + contact.lastName);
                    $scope.allContacts.splice(i, 1);
                    $scope.isClientBusy = false;
                }, function errorCallBack(response) {
                    if (response.status == -1) {
                        updateAlert("alert-warning", "Couldn't establish connection with server. Please try again later.");
                    } else {
                        updateAlert("alert-danger", "Error: " + response.status + " " + response.statusText);
                    }
                    $scope.isClientBusy = false;
                });
            resetTmpContact();
            $location.path('');
        }
    }

    /**
     * Model which represents search request.
     */
    $scope.search = {
        query: "",
        status: "",
        result: []
    };

    /**
     * Function which sends out GET request with search query.
     */
    $scope.searchContacts = function () {
        $scope.isClientBusy = true;
        $scope.search.status = "Please wait..."
        $http.get(url + "/Search/" + $scope.search.query)
            .then(function successCallBack(response) {
                $scope.search.result = response.data;
                $scope.search.status = response.data.length + " contacts found.";
                $scope.isClientBusy = false;
            }, function errorCallBack(response) {
                $scope.search.status = "Oops! Something went wrong. (" + response.status + response.statusText + ")";
                $scope.isClientBusy = false;
            });
    };

    /**
     * Represents selected contacts
     */
    $scope.selected = [];

    /**
     * Depending on isContact flag and action it updates selected contacts
     * or adds or removes tags from temporaryContact.
     * @param {*} action 
     * @param {*} par 
     * @param {*} isContact 
     */
    function updateSelected(action, par, isContact) {
        if (isContact && action == "add" && $scope.selected.indexOf(par) == -1) {
            $scope.selected.push(par);
        }
        if (isContact && action == "remove" && $scope.selected.indexOf(par) !== -1) {
            $scope.selected.splice($scope.selected.indexOf(par), 1);
        }

        if (!isContact && action == "add" && tagIndex(par) == - 1) {
            $scope.temporaryContact.tags.push(par);
        }
        if (!isContact && action == "remove" && tagIndex(par) !== -1) {
            $scope.temporaryContact.tags.splice(tagIndex(par), 1);
        }
    }

    /**
     * Replaces temporaryContact.tags.indexOf(tag) becouse it
     * always returns -1
     * @param {*} tag 
     */
    function tagIndex(tag) {
        for (var i = 0; i < $scope.temporaryContact.tags.length; i++)
            if ($scope.temporaryContact.tags[i].id == tag.id) break;
        return i < $scope.temporaryContact.tags.length ? i : -1;
    }

    /**
     * Function which is bound to every checkbox for every contact 
     * or tag, depending on isContact flag.
     * When checkbox is clicked, this function will be called and depending
     * on whether it is checked or no it will call "updateSelected" function to
     * add or remove contact from "selected" contacts array or
     * add or remove tag from temporaryContact.tags array.
     */
    $scope.updateSelection = function (par, $event, isContact) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? "add" : "remove");
        updateSelected(action, par, isContact);
    }

    /**
     * Function which is called when select all checkbox is clicked. 
     * It selects or deselects every contact in "allContacts" array using
     * "updateSelected" function.
     */
    $scope.selectAll = function ($event) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? "add" : "remove");
        for (var i = 0; i < $scope.allContacts.length; i++) {
            updateSelected(action, $scope.allContacts[i]);
        }
    }

    /**
     * Used to determine if checkbox for each contact is checked
     */
    $scope.isSelected = function (par, isContact) {
        if (isContact) {
            return $scope.selected.indexOf(par) !== -1;
        } else {
            for (var i = 0; i < $scope.temporaryContact.tags.length; i++)
                if ($scope.temporaryContact.tags[i].id == par.id) break
            return i < $scope.temporaryContact.tags.length;
        }

    }

    /**
     * Used to determine if "selectAll" checkbox is checked.
     */
    $scope.isAllSelected = function () {
        return $scope.selected.length == $scope.allContacts.length && $scope.allContacts.length !== 0;
    }

    /**
     * Sends out PUT request to bookmark selected contacts. 
     * It changes "isClientBusy" flag until response arives.
     * User is alerted if response is successfull or failed.
     */
    $scope.bookmarkSelected = function () {
        $scope.isClientBusy = true;
        updateAlert("alert-info", "Please wait...");
        var contacts = angular.copy($scope.selected);
        for (var i = 0; i < contacts.length; i++) {
            if (!contacts[i].isBookmarked) {
                contacts[i].isBookmarked = true;
            }
        }
        $http.put(url + "/UpdateMany", angular.copy(contacts), {
            headers: { 'Content-Type': 'application/json' }
        }).then(function successCallBack(response) {
            return $http.get(url);
        })
            .then(function successCallBack(response) {
                $scope.allContacts = [];
                $scope.allContacts = response.data;
                updateAlert("alert-success", "Successfully updated selected contacts");
                $scope.selected = [];
                $scope.isClientBusy = false;
            })
            .catch(function errorCallBack(response) {
                if (response.status == -1) {
                    updateAlert("alert-warning", "Couldn't establish connection with server. Please try again later.");
                } else {
                    updateAlert("alert-danger", "Error: " + response.status + " " + response.statusText);
                }
                $scope.selected = [];
                $scope.isClientBusy = false;
            });
    }

    /**
     * Sends out PUT request to delete selected contacts. 
     * It changes "isClientBusy" flag until response arives.
     * User is alerted if response is successfull or failed.
     */
    $scope.deleteSelected = function () {

        var alert = $window.confirm("Are you sure you want to delete selected contacts?");
        if (alert) {
            $scope.isClientBusy = true;
            updateAlert("alert-info", "Please wait...");

            $http.post(url + "/DeleteMany", angular.copy($scope.selected), {
                headers: { 'Content-Type': 'application/json' }
            }).then(function successCallBack(response) {
                return $http.get(url);
            })
                .then(function successCallBack(response) {
                    updateAlert("alert-success", "Successfully deleted selected contacts");
                    $scope.allContacts = response.data;
                    $scope.selected = [];
                    $scope.isClientBusy = false;
                })
                .catch(function errorCallBack(response) {
                    if (response.status == -1) {
                        updateAlert("alert-warning", "Couldn't establish connection with server. Please try again later.");
                    } else {
                        updateAlert("alert-danger", "Error: " + response.status + " " + response.statusText);
                    }
                    $scope.selected = [];
                    $scope.isClientBusy = false;
                });
        }
    }

    /**
     * Gets called when URL changes and
     * enables back and forward buttons in browser.
     */
    $scope.$on("$locationChangeStart", function () {
        var path = $location.absUrl();
        if (path.endsWith('index.html') ||
            path.endsWith('#!') ||
            path.endsWith('#!/')) {
            $scope.discardChanges();
        }
        else {
            var id = path.slice(path.lastIndexOf('/') + 1);
            $scope.contactDetails(id);
        }
    });

    $scope.tags = [];

    /**
     * Gets all tags from server and stores them into "tags"
     */
    $scope.getAllTags = function () {
        $scope.search.status = 'Please wait...'
        $('#tagsModal').modal('show');
        $http.get(url + "/Tags").then(function successCallBack(response) {
            $scope.tags = response.data;
            $scope.search.status = response.data.length + ' tags found.'
        }, function errorCallBack(response) {
            $scope.closeTagsModal();
            if (response.status == -1) {
                updateAlert("alert-warning", "Couldn't establish connection with server. Please try again later.");
            } else {
                updateAlert("alert-danger", "Error: " + response.status + " " + response.statusText);
            }
        });
    };

    /**
     * Sends out HTTP Delete request to delete desired tag
     */
    $scope.deleteTag = function (id) {
        $scope.isClientBusy = true;
        $http.delete(url + "/DeleteTag/" + id).then(function successCallBack(response) {
            for (var i = 0; i < $scope.tags.length; i++)
                if ($scope.tags[i].id == id) break;
            $scope.tags.splice(i, 1);
            $scope.search.status = 'Successfully deleted tag.'
            $scope.isClientBusy = false;
        }, function errorCallBack(response) {
            $scope.closeTagsModal();
            if (response.status == -1) {
                updateAlert("alert-warning", "Couldn't establish connection with server. Please try again later.");
            } else {
                updateAlert("alert-danger", "Error: " + response.status + " " + response.statusText);
            }
            $scope.isClientBusy = false;
        });
    };

    /**
     * Sends out HTTP Put request to update desired tag
     */
    $scope.updateTag = function (tag) {
        $scope.isClientBusy = true;
        $http.put(url + "/UpdateTag", tag, {
            headers: { 'Content-Type': 'application/json' }
        }).then(function successCallBack(response) {
            $scope.search.status = 'Successfully updated ' + response.data.name + ' tag.'
            $scope.isClientBusy = false;
        }, function errorCallBack(response) {
            $scope.closeTagsModal();
            if (response.status == -1) {
                updateAlert("alert-warning", "Couldn't establish connection with server. Please try again later.");
            } else {
                updateAlert("alert-danger", "Error: " + response.status + " " + response.statusText);
            }
            $scope.isClientBusy = false;
        });
    };

    $scope.newTag = "";

    /**
     * Sends out HTTP Post request to create new tag
     */
    $scope.createTag = function () {
        $scope.isClientBusy = true;
        $http.post(url + "/CreateTag/" + $scope.newTag, $scope.newTag, {
            headers: { 'Content-Type': 'application/json' }
        }).then(function successCallBack(response) {
            if (response.status !== 204) {
                $scope.tags.push(response.data);
                $scope.search.status = 'Successfully added new tag'
                $scope.newTag = "";
            } else {
                $scope.search.status = 'Please enter tag name'
                $scope.newTag = "";
            }
            $scope.isClientBusy = false;
        }, function errorCallBack(response) {
            $scope.closeTagsModal();
            if (response.status == -1) {
                updateAlert("alert-warning", "Couldn't establish connection with server. Please try again later.");
            } else {
                updateAlert("alert-danger", "Error: " + response.status + " " + response.statusText);
            }
            $scope.isClientBusy = false;
        });
        $scope.newTag = "";
    }

    $scope.closeTagsModal = function () {
        $scope.tags = [];
        $scope.search.status = '';
        $scope.newTag = "";
        $('#tagsModal').modal('hide');
    };

});
