﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ContactListBackend.Models;
using ContactListBackend.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace ContactListBackend
{
    public class Startup
    {

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigins",
                    builder =>
                    {
                        builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials();
                    });
            });

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAllOrigins"));
            });

            services.AddTransient<IContactsRepository, ContactsSqlRepository>(
                s => new ContactsSqlRepository(Configuration.GetConnectionString("DefaultConnection")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IServiceProvider serviceProvider)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseCors("AllowAllOrigins");

            app.UseMvc();

            var connection = new SqlConnection(Configuration.GetConnectionString("DefaultConnection"));
            var command = new SqlCommand("SELECT * FROM Contacts", connection);
            connection.Open();
            var reader = command.ExecuteReader();
            var flag = !reader.HasRows;
            reader.Close();
            if (flag) SeedDatabase(connection);

            connection.Close();
        }

        private static void SeedDatabase(SqlConnection connection)
        {
            #region New SqlCommand
            var command = new SqlCommand(
                "INSERT INTO Contacts(Id, FirstName, LastName, Nickname, Address, IsBookmarked)\r\n" +
                "VALUES (\'3b4ce856-3126-4c29-ad10-7bde63707c2d\', \'Matija\', \'Puzjak\', null, \'Bistra\', 1), \r\n" +
                "(\'6e27f58a-f8f9-4b2c-b15b-d572a1c3b8f5\', \'Ivan\', \'Puzjak\', \'puzi\', null, 0), \r\n" +
                "(\'ff723f2e-7e5b-473d-b958-08b16ef0ba9b\', \'Mislav\', null, \'muki\', null, 1), \r\n" +
                "(\'50e4cf4e-6847-4686-b16b-2bae3374c74a\', \'Kristina\', \'Šporević\', \'12\', \'Birava\', 1), \r\n" +
                "(\'dcdb12a1-b2eb-454f-811b-59085ac2b4a6\', \'Lucija\', null, \'lucz\', null, 0), \r\n" +
                "(\'b2597665-5e19-48d2-9aed-ac87edfbac73\', \'Karlo\', \'Droga\', null, null, 0), \r\n" +
                "(\'307bc568-b1ab-496a-9a50-673585da76c6\', \'Janj\', null, \'krava\', \'selo\', 1);\r\n\r\n" +
                "INSERT INTO PhoneNumbers(Id, OwnerId, PhoneNumber)\r\n" +
                "VALUES (\'f4909429-cea1-4323-9489-47f2449e7c8f\', \'3b4ce856-3126-4c29-ad10-7bde63707c2d\', \'0958884444\'), \r\n" +
                "(\'1e3929d6-961c-45b7-a52e-1d23fedf1096\', \'3b4ce856-3126-4c29-ad10-7bde63707c2d\', \'3390282\'), \r\n" +
                "(\'df34a878-08fc-42ff-87f5-472b01eacf9c\', \'ff723f2e-7e5b-473d-b958-08b16ef0ba9b\', \'1234567\'), \r\n" +
                "(\'b08653da-c0aa-4957-8a97-221dd70d1ec9\', \'307bc568-b1ab-496a-9a50-673585da76c6\', \'1234567\');\r\n\r\n" +
                "INSERT INTO Emails(Id, OwnerId, Email)\r\n" +
                "VALUES (\'5556d108-74b1-4ff5-b72a-b60e9f265951\', \'3b4ce856-3126-4c29-ad10-7bde63707c2d\', \'majata@gmgmg.ssd\'), \r\n" +
                "(\'13e95981-9dcb-4bb9-9c9d-0c0b4b4682dd\', \'6e27f58a-f8f9-4b2c-b15b-d572a1c3b8f5\', \'ivanmail@mail.ivan\'), \r\n" +
                "(\'97d63e11-4b76-49e3-8d23-a28b935e32d7\', \'dcdb12a1-b2eb-454f-811b-59085ac2b4a6\', \'lucz1234@gdsas.asss\');\r\n\r\n\r\n" +
                "INSERT INTO Tags(Id, Tag)\r\nVALUES (\'619b941b-9d65-448e-9ad9-c949727260f4\', \'fam\'), \r\n" +
                "(\'5d46bf95-fa7c-4436-87be-20118260799f\', \'frend\');\r\n\r\n" +
                "INSERT INTO ContactTag(ContactId, TagId)\r\n" +
                "VALUES (\'3b4ce856-3126-4c29-ad10-7bde63707c2d\', \'619b941b-9d65-448e-9ad9-c949727260f4\'), \r\n" +
                "(\'3b4ce856-3126-4c29-ad10-7bde63707c2d\', \'5d46bf95-fa7c-4436-87be-20118260799f\'), \r\n" +
                "(\'50e4cf4e-6847-4686-b16b-2bae3374c74a\', \'5d46bf95-fa7c-4436-87be-20118260799f\'), \r\n" +
                "(\'6e27f58a-f8f9-4b2c-b15b-d572a1c3b8f5\', \'619b941b-9d65-448e-9ad9-c949727260f4\'), \r\n" +
                "(\'ff723f2e-7e5b-473d-b958-08b16ef0ba9b\', \'619b941b-9d65-448e-9ad9-c949727260f4\');\r\n",
                connection);
            #endregion

            command.ExecuteNonQuery();
        }


    }
}
