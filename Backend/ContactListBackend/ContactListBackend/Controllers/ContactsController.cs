using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactListBackend.Exceptions;
using ContactListBackend.Models;
using ContactListBackend.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ContactListBackend.Controllers
{
    [Produces("application/json")]
    [Route("api/Contacts")]
    public class ContactsController : Controller
    {
        private readonly IContactsRepository _repository;

        public ContactsController(IContactsRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetContact(Guid id)
        {
            try
            {
                return Ok(await _repository.Get(id));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllContacts()
        {
            try
            {
                return Ok(await _repository.GetAllContacts());
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateContact([FromBody] Contact contact)
        {
            try
            {
                await _repository.Update(contact);
                return Ok(contact);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContact(string id)
        {
            try
            {
                var flag = await _repository.Delete(new Guid(id));
                if (flag) return Ok();
                return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddContact([FromBody] Contact contact)
        {
            try
            {
                var newContact = new Contact(contact);
                await _repository.Add(newContact);
                return Ok(newContact);
            }
            catch (DuplicateContactException)
            {
                return BadRequest("Contact already exists");
            }
        }


        [HttpGet("Search/{searchQuerry}")]
        public async Task<IActionResult> Search(string searchQuerry)
        {
            try
            {
                return Ok(await _repository.Search(searchQuerry));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("UpdateMany")]
        public async Task<IActionResult> UpdateMany([FromBody] List<Contact> contacts)
        {
            try
            {
                await _repository.UpdateMany(contacts);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("DeleteMany")]
        public async Task<IActionResult> DeleteMany([FromBody] List<Contact> contacts)
        {
            try
            {
                await _repository.DeleteMany(contacts);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("Tags")]
        public async Task<IActionResult> GetAllTags()
        {
            try
            {
                return Ok(await _repository.GetAllTags());
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("UpdateTag")]
        public async Task<IActionResult> UpdateTag([FromBody] Tag tag)
        {
            try
            {
                await _repository.UpdateTag(tag);
                return Ok(tag);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("DeleteTag/{id}")]
        public async Task<IActionResult> DeleteTag(Guid id)
        {
            try
            {
                await _repository.DeleteTag(id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("CreateTag/{name}")]
        public async Task<IActionResult> CreateTag(string name)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name)) return NoContent();
                var tag = new Tag(name);
                await _repository.CreateTag(tag);
                return Ok(tag);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}