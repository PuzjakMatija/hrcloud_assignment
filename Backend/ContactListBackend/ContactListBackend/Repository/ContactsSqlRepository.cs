﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactListBackend.Exceptions;
using ContactListBackend.Models;
using Microsoft.EntityFrameworkCore;

namespace ContactListBackend.Repository
{
    public class ContactsSqlRepository : IContactsRepository
    {
        private readonly SqlConnection _connection;

        public ContactsSqlRepository(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
        }

        /// <summary>
        /// Used to convert SqlDataReader to Contact. 
        /// Reader must be in correct order for this method to work. 
        /// The order is Contact(Id, FirstName, LastName, Nickname, Address, IsBookmarked)
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private static Contact ParseContact(SqlDataReader reader)
        {
            return new Contact()
            {
                Id = (Guid)reader[0],
                FirstName = (string)reader[1],
                LastName = (reader[2] != DBNull.Value) ? (string)reader[2] : string.Empty,
                Nickname = (reader[3] != DBNull.Value) ? (string)reader[3] : string.Empty,
                Address = (reader[4] != DBNull.Value) ? (string)reader[4] : string.Empty,
                IsBookmarked = (reader[5] != DBNull.Value) ? (bool)reader[5] : false,
                PhoneNumbers = new List<PhoneNumber>(),
                Emails = new List<Email>(),
                Tags = new List<Tag>()
            };
        }

        public async Task Add(Contact contact)
        {
            // Command string builder is used to merge multiple SQL queries into one string.
            // Depending on whether contact has phone numbers, emails or tags, new queries
            // will be added.
            var commandStringBuilder = new StringBuilder();

            if (contact.PhoneNumbers.Count > 0)
            {
                commandStringBuilder.AppendLine("INSERT INTO PhoneNumbers(Id, OwnerId, PhoneNumber) VALUES ");
                foreach (var phoneNumber in contact.PhoneNumbers)
                {
                    if (phoneNumber.Id == Guid.Empty) phoneNumber.Id = Guid.NewGuid();
                    commandStringBuilder.AppendFormat($"('{phoneNumber.Id}', '{contact.Id}', '{phoneNumber.Number}'),");
                }
                commandStringBuilder.Remove(commandStringBuilder.Length - 1, 1);
                commandStringBuilder.Append("; ");
            }

            if (contact.Emails.Count > 0)
            {
                commandStringBuilder.AppendLine("INSERT INTO Emails(Id, OwnerId, Email) VALUES");
                foreach (var email in contact.Emails)
                {
                    if (email.Id == Guid.Empty) email.Id = Guid.NewGuid();
                    commandStringBuilder.AppendFormat($"('{email.Id}', '{contact.Id}', '{email.Address}'),");
                }
                commandStringBuilder.Remove(commandStringBuilder.Length - 1, 1);
                commandStringBuilder.Append("; ");
            }

            if (contact.Tags.Count > 0)
            {
                commandStringBuilder.AppendLine("INSERT INTO ContactTag(ContactId, TagId) VALUES ");
                foreach (var tag in contact.Tags)
                {
                    commandStringBuilder.AppendFormat($"('{contact.Id}', '{tag.Id}'),");
                }
                commandStringBuilder.Remove(commandStringBuilder.Length - 1, 1);
                commandStringBuilder.Append("; ");
            }

            var command = new SqlCommand($"INSERT INTO Contacts(Id, FirstName, LastName, Nickname, Address, IsBookmarked) " +
                                         $"VALUES ('{contact.Id}', '{contact.FirstName}', '{contact.LastName}', '{contact.Nickname}', " +
                                         $"'{contact.Address}', '{contact.IsBookmarked}'); " +
                                         $"{commandStringBuilder}", _connection);
            await _connection.OpenAsync();
            await command.ExecuteNonQueryAsync();
            _connection.Close();
        }

        public async Task<Contact> Get(Guid id)
        {
            var command = new SqlCommand($"SELECT * FROM Contacts WHERE Id = '{id}'", _connection);
            await _connection.OpenAsync();
            var reader = await command.ExecuteReaderAsync();
            if (!reader.HasRows)
            {
                reader.Close();
                _connection.Close();
                return null;
            }
            reader.Read();
            var contact = ParseContact(reader);
            reader.Close();
            command = new SqlCommand($"SELECT PhoneNumbers.Id, PhoneNumber " +
                                     $"FROM Contacts JOIN PhoneNumbers ON Contacts.Id = PhoneNumbers.OwnerId " +
                                     $"WHERE Contacts.Id = '{id}'", _connection);
            reader = await command.ExecuteReaderAsync();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    contact.PhoneNumbers.Add(new PhoneNumber()
                    {
                        Id = (Guid)reader[0],
                        Number = (string)reader[1]
                    });
                }
            }
            reader.Close();

            command = new SqlCommand($"SELECT Emails.Id, Email " +
                                     $"FROM Contacts JOIN Emails ON Contacts.Id = Emails.OwnerId " +
                                     $"WHERE Contacts.Id = '{id}'", _connection);
            reader = await command.ExecuteReaderAsync();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    contact.Emails.Add(new Email()
                    {
                        Id = (Guid)reader[0],
                        Address = (string)reader[1]
                    });
                }

            }
            reader.Close();

            command = new SqlCommand($"SELECT Tags.Id, Tag " +
                                     $"FROM Contacts JOIN ContactTag ON Contacts.Id = ContactTag.ContactId " +
                                     $"JOIN Tags ON Tags.Id = ContactTag.TagId " +
                                     $"WHERE Contacts.Id = '{id}'", _connection);
            reader = await command.ExecuteReaderAsync();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    contact.Tags.Add(new Tag()
                    {
                        Id = (Guid)reader[0],
                        Name = (string)reader[1]
                    });
                }
            }
            reader.Close();

            _connection.Close();


            return contact;

        }

        public async Task<bool> Delete(Guid id)
        {
            var command = new SqlCommand($"DELETE FROM ContactTag WHERE ContactId = '{id}'; " +
                                         $"DELETE FROM Emails WHERE OwnerId = '{id}'; " +
                                         $"DELETE FROM PhoneNumbers WHERE OwnerId = '{id}'; " +
                                         $"DELETE FROM Contacts WHERE Id = '{id}';", _connection);
            await _connection.OpenAsync();
            var n = await command.ExecuteNonQueryAsync();
            _connection.Close();
            return n >= 1;
        }

        public async Task DeleteMany(IEnumerable<Contact> contacts)
        {
            var ids = new StringBuilder();
            foreach (var contact in contacts)
            {
                ids.AppendFormat($"'{contact.Id}', ");
            }
            ids.Remove(ids.Length - 2, 2);
            var command = new SqlCommand($"DELETE FROM ContactTag WHERE ContactId IN ({ids}); " +
                                         $"DELETE FROM Emails WHERE OwnerId IN ({ids}); " +
                                         $"DELETE FROM PhoneNumbers WHERE OwnerId IN ({ids}); " +
                                         $"DELETE FROM Contacts WHERE Id IN ({ids});", _connection);
            await _connection.OpenAsync();
            await command.ExecuteNonQueryAsync();
            _connection.Close();
        }

        public async Task Update(Contact contact)
        {
            // These string builders store id's of phone numbers, emails and tags
            // which the updated contact has. If there are numbers, emails or tags 
            // in database which don't have any of these id's, they will be deleted.
            var phoneNumberIds = new StringBuilder();
            var emailIds = new StringBuilder();
            var tagIds = new StringBuilder();

            // This string builder is used to store INSERT or UPDATE commands depending if
            // phone numbers or emails already exist in database.
            var insertOrUpdateCommands = new StringBuilder();

            foreach (var phoneNumber in contact.PhoneNumbers)
            {
                if (phoneNumber.Id != Guid.Empty)
                    phoneNumberIds.AppendFormat($"'{phoneNumber.Id}',");

                insertOrUpdateCommands.AppendFormat(
                    $"IF (SELECT COUNT(*) FROM PhoneNumbers WHERE Id = '{phoneNumber.Id}') = 0 " +
                    $"INSERT INTO PhoneNumbers(Id, PhoneNumber, OwnerId) " +
                    $"VALUES ('{Guid.NewGuid()}', '{phoneNumber.Number}', '{contact.Id}') " +
                    $"ELSE " +
                    $"UPDATE PhoneNumbers " +
                    $"SET PhoneNumber = '{phoneNumber.Number}' " +
                    $"WHERE Id = '{phoneNumber.Id}'; ");

            }
            foreach (var email in contact.Emails)
            {
                if (email.Id != Guid.Empty)
                    emailIds.AppendFormat($"'{email.Id}',");

                insertOrUpdateCommands.AppendFormat(
                    $"IF (SELECT COUNT(*) FROM Emails WHERE Id = '{email.Id}') = 0 " +
                    $"INSERT INTO Emails(Id, Email, OwnerId) " +
                    $"VALUES ('{Guid.NewGuid()}', '{email.Address}', '{contact.Id}') " +
                    $"ELSE " +
                    $"UPDATE Emails " +
                    $"SET Email = '{email.Address}' " +
                    $"WHERE Id = '{email.Id}'; ");

            }
            foreach (var tag in contact.Tags)
            {
                tagIds.AppendFormat($"'{tag.Id}',");
                insertOrUpdateCommands.AppendFormat(
                    $"IF (SELECT COUNT(*) FROM ContactTag WHERE ContactId = '{contact.Id}' AND TagId = '{tag.Id}') = 0 " +
                    $"INSERT INTO ContactTag(ContactId, TagId) " +
                    $"VALUES ('{contact.Id}', '{tag.Id}'); ");
            }

            var command = new SqlCommand($"UPDATE Contacts " +
                                         $"SET FirstName = '{contact.FirstName}', LastName = '{contact.LastName}', " +
                                         $"Nickname = '{contact.Nickname}', Address = '{contact.Address}', IsBookmarked = '{contact.IsBookmarked}' " +
                                         $"WHERE Id = '{contact.Id}'; ", _connection);
            if (tagIds.Length == 0)
            {
                command.CommandText += $"DELETE FROM ContactTag WHERE ContactId = '{contact.Id}'; ";
            }
            else
            {
                tagIds.Remove(tagIds.Length - 1, 1);
                command.CommandText +=
                    $"DELETE FROM ContactTag WHERE ContactId = '{contact.Id}' AND TagId NOT IN ({tagIds}); ";
            }

            if (phoneNumberIds.Length == 0)
            {
                command.CommandText += $"DELETE FROM PhoneNumbers WHERE OwnerId = '{contact.Id}'; ";
            }
            else
            {
                phoneNumberIds.Remove(phoneNumberIds.Length - 1, 1);
                command.CommandText +=
                    $"DELETE FROM PhoneNumbers WHERE OwnerId = '{contact.Id}' AND Id NOT IN ({phoneNumberIds}); ";
            }

            if (emailIds.Length == 0)
            {
                command.CommandText += $"DELETE FROM Emails WHERE OwnerId = '{contact.Id}'; ";
            }
            else
            {
                emailIds.Remove(emailIds.Length - 1, 1);
                command.CommandText +=
                    $"DELETE FROM Emails WHERE OwnerId = '{contact.Id}' AND Id NOT IN ({emailIds}); ";
            }

            command.CommandText += $"{insertOrUpdateCommands}";

            await _connection.OpenAsync();
            await command.ExecuteNonQueryAsync();
            _connection.Close();


        }

        public async Task UpdateMany(IEnumerable<Contact> contacts)
        {
            foreach (var contact in contacts)
            {
                await Update(contact);
            }
        }

        public async Task<List<Contact>> GetAllContacts()
        {
            var command = new SqlCommand("SELECT * FROM Contacts", _connection);
            await _connection.OpenAsync();
            var reader = await command.ExecuteReaderAsync();
            var contacts = new List<Contact>();
            try
            {
                while (reader.Read())
                {
                    contacts.Add(ParseContact(reader));
                }
            }
            finally
            {
                reader.Close();
                _connection.Close();
            }
            return contacts;
        }

        public async Task<IList<Contact>> Search(string searchQuerry)
        {
            searchQuerry = searchQuerry.ToLowerInvariant();
            var contacts = new List<Contact>();
            var command = new SqlCommand($"SELECT DISTINCT Contacts.Id, FirstName, LastName, Nickname, Address, IsBookmarked " +
                                         $"FROM Contacts LEFT JOIN PhoneNumbers ON Contacts.Id = PhoneNumbers.OwnerId " +
                                         $"LEFT JOIN Emails ON Contacts.Id = Emails.OwnerId " +
                                         $"LEFT JOIN ContactTag ON Contacts.Id = ContactTag.ContactId " +
                                         $"LEFT JOIN Tags ON ContactTag.TagId = Tags.Id " +
                                         $"WHERE LOWER(FirstName) LIKE '%{searchQuerry}%' OR " +
                                         $"LOWER(LastName) LIKE '%{searchQuerry}%' OR " +
                                         $"LOWER(Nickname) LIKE '%{searchQuerry}%' OR " +
                                         $"LOWER(Address) LIKE '%{searchQuerry}%' OR " +
                                         $"LOWER(PhoneNumber) LIKE '%{searchQuerry}%' OR " +
                                         $"LOWER(Email) LIKE '%{searchQuerry}%' OR " +
                                         $"LOWER(Tag) LIKE '%{searchQuerry}%';", _connection);
            await _connection.OpenAsync();

            var reader = await command.ExecuteReaderAsync();
            try
            {
                while (reader.Read())
                {
                    contacts.Add(ParseContact(reader));
                }
            }
            finally
            {
                reader.Close();
                _connection.Close();
            }
            return contacts;

        }

        public async Task<IList<Tag>> GetAllTags()
        {
            var tags = new List<Tag>();
            var command = new SqlCommand("SELECT * FROM Tags", _connection);
            await _connection.OpenAsync();
            var reader = await command.ExecuteReaderAsync();

            try
            {
                while (reader.Read())
                {
                    tags.Add(new Tag()
                    {
                        Id = (Guid)reader[0],
                        Name = (string)reader[1]
                    });
                }
            }
            finally
            {
                reader.Close();
                _connection.Close();
            }
            return tags.Count != 0 ? tags : null;
        }

        public async Task UpdateTag(Tag tag)
        {
            var command = new SqlCommand($"UPDATE Tags SET Tag = '{tag.Name}' WHERE Id = '{tag.Id}'", _connection);
            await _connection.OpenAsync();
            await command.ExecuteNonQueryAsync();
            _connection.Close();
        }

        public async Task DeleteTag(Guid id)
        {
            var command = new SqlCommand($"DELETE FROM ContactTag WHERE TagId = '{id}'; " +
                                         $"DELETE FROM Tags WHERE Id = '{id}';", _connection);
            await _connection.OpenAsync();
            await command.ExecuteNonQueryAsync();
            _connection.Close();
        }

        public async Task CreateTag(Tag tag)
        {

            var command = new SqlCommand($"INSERT INTO Tags(Id, Tag)" +
                                         $"VALUES ('{tag.Id}', '{tag.Name}')", _connection);
            await _connection.OpenAsync();
            await command.ExecuteNonQueryAsync();
            _connection.Close();

        }
    }
}
