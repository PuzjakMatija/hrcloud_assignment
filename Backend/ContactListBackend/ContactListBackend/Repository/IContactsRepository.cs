﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using ContactListBackend.Models;

namespace ContactListBackend.Repository
{
    public interface IContactsRepository
    {
        /// <summary>
        /// Adds new contact to repository. If contact already exists 
        /// DuplicateContactException is thrown.
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        Task Add(Contact contact);

        /// <summary>
        /// Gets contact by id from repository.
        /// Returns Contact if found, null otherwise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Contact> Get(Guid id);

        /// <summary>
        /// Deletes contact by id from repository.
        /// Returns true if user is found and deleted, false otherwise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> Delete(Guid id);

        /// <summary>
        /// Deletes many contacts from repository. 
        /// If any contact isn't found, it will be ignored.
        /// </summary>
        /// <param name="contacts"></param>
        Task DeleteMany(IEnumerable<Contact> contacts);

        /// <summary>
        /// Updates contact in repository. 
        /// If contact doesnt exist, it will be added.
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        Task Update(Contact contact);

        /// <summary>
        /// Updates many contacts in repository. 
        /// If any contact isn't found, it will be added.
        /// </summary>
        /// <param name="contacts"></param>
        Task UpdateMany(IEnumerable<Contact> contacts);

        /// <summary>
        /// Returns all contacts from repository.
        /// </summary>
        /// <returns></returns>
        Task<List<Contact>> GetAllContacts();

        /// <summary>
        /// Returns all contacts which satisfy search query
        /// </summary>
        /// <param name="searchQuery"></param>
        /// <returns></returns>
        Task<IList<Contact>> Search(string searchQuery);

        /// <summary>
        /// Returns list of all tags from repository
        /// </summary>
        /// <returns></returns>
        Task<IList<Tag>> GetAllTags();

        /// <summary>
        /// Updates existing tag in repository
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        Task UpdateTag(Tag tag);

        /// <summary>
        /// Deletes existing tag from repository
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteTag(Guid id);

        /// <summary>
        /// Creates new tag in repository
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        Task CreateTag(Tag tag);
    }
}