﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ContactListBackend.Models
{
    /// <summary>
    /// Contact model which is used to transport data from client application to backend
    /// and in other direction. It is also used to represent data stored in database.
    /// </summary>
    public class Contact : IEquatable<Contact>, IComparable<Contact>
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Nickname { get; set; }
        public string Address { get; set; }
        public List<PhoneNumber> PhoneNumbers { get; set; }
        public List<Email> Emails { get; set; }
        public List<Tag> Tags { get; set; }
        public bool IsBookmarked { get; set; }

        #region Constructors
        public Contact()
        {

        }

        public Contact(string firstName, string lastName, string nickname, string address, List<PhoneNumber> numbers, List<Email> emails, List<Tag> tags, bool isBookmarked)
        {
            Id = Guid.NewGuid();
            FirstName = firstName;
            LastName = lastName;
            Nickname = nickname;
            Address = address;
            PhoneNumbers = numbers;
            Emails = emails;
            Tags = tags;
            IsBookmarked = isBookmarked;
        }

        public Contact(Contact contact)
        {
            Id = Guid.NewGuid();
            FirstName = contact.FirstName;
            LastName = contact.LastName;
            Nickname = contact.Nickname;
            Address = contact.Address;
            PhoneNumbers = contact.PhoneNumbers;
            Emails = contact.Emails;
            Tags = contact.Tags;
            IsBookmarked = contact.IsBookmarked;
        } 
        #endregion

        #region Equals, CompareTo
        public bool Equals(Contact other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Id, other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Contact)obj);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode() : 0);
        }

        public int CompareTo(Contact other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            var firstNameComparison = string.Compare(FirstName, other.FirstName, StringComparison.Ordinal);
            if (firstNameComparison != 0) return firstNameComparison;
            var lastNameComparison = string.Compare(LastName, other.LastName, StringComparison.Ordinal);
            if (lastNameComparison != 0) return lastNameComparison;
            return string.Compare(Nickname, other.Nickname, StringComparison.Ordinal);
        } 
        #endregion
    }
}
