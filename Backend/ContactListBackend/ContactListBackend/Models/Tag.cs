﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactListBackend.Models
{
    /// <summary>
    /// Represents tags stored in database.
    /// </summary>
    public class Tag : IEquatable<Tag>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public Tag()
        {
            
        }

        public Tag(string name)
        {
            Id = Guid.NewGuid();
            Name = name;
        }

        public bool Equals(Tag other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id.Equals(other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Tag) obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
