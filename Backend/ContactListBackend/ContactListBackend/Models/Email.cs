﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactListBackend.Models
{
    /// <summary>
    /// Represents emails stored in database.
    /// </summary>
    public class Email : IEquatable<Email>
    {
        public Guid Id { get; set; }
        public string Address { get; set; }

        public Email()
        {
            
        }

        public Email(string address)
        {
            Id = Guid.NewGuid();
            Address = address;
        }

        public bool Equals(Email other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id.Equals(other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Email) obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
