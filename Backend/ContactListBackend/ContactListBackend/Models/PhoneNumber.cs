﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactListBackend.Models
{
    /// <summary>
    /// Represents phone numbers stored in database.
    /// </summary>
    public class PhoneNumber : IEquatable<PhoneNumber>
    {
        public Guid Id { get; set; }
        public string Number { get; set; }

        public PhoneNumber()
        {
            
        }

        public PhoneNumber(string number)
        {
            Id = Guid.NewGuid();
            Number = number;
        }

        public bool Equals(PhoneNumber other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id.Equals(other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PhoneNumber) obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
