﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactListBackend.Exceptions
{
    public class DuplicateContactException : Exception
    {
        public DuplicateContactException() : base()
        {
            
        }

        public DuplicateContactException(string message) : base(message)
        {
            
        }

        public DuplicateContactException(string message, Exception innerException) : base(message, innerException)
        {
            
        }
    }
}
