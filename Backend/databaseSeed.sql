INSERT INTO Contacts(Id, FirstName, LastName, Nickname, Address, IsBookmarked)
VALUES ('3b4ce856-3126-4c29-ad10-7bde63707c2d', 'Matija', 'Puzjak', null, 'Bistra', 1), 
('6e27f58a-f8f9-4b2c-b15b-d572a1c3b8f5', 'Ivan', 'Puzjak', 'puzi', null, 0), 
('ff723f2e-7e5b-473d-b958-08b16ef0ba9b', 'Mislav', null, 'muki', null, 1), 
('50e4cf4e-6847-4686-b16b-2bae3374c74a', 'Kristina', 'Šporević', '12', 'Birava', 1), 
('dcdb12a1-b2eb-454f-811b-59085ac2b4a6', 'Lucija', null, 'lucz', null, 0), 
('b2597665-5e19-48d2-9aed-ac87edfbac73', 'Karlo', 'Droga', null, null, 0), 
('307bc568-b1ab-496a-9a50-673585da76c6', 'Janj', null, 'krava', 'selo', 1);

INSERT INTO PhoneNumbers(Id, OwnerId, PhoneNumber)
VALUES ('f4909429-cea1-4323-9489-47f2449e7c8f', '3b4ce856-3126-4c29-ad10-7bde63707c2d', '0958884444'), 
('1e3929d6-961c-45b7-a52e-1d23fedf1096', '3b4ce856-3126-4c29-ad10-7bde63707c2d', '3390282'), 
('df34a878-08fc-42ff-87f5-472b01eacf9c', 'ff723f2e-7e5b-473d-b958-08b16ef0ba9b', '1234567'), 
('b08653da-c0aa-4957-8a97-221dd70d1ec9', '307bc568-b1ab-496a-9a50-673585da76c6', '1234567');

INSERT INTO Emails(Id, OwnerId, Email)
VALUES ('5556d108-74b1-4ff5-b72a-b60e9f265951', '3b4ce856-3126-4c29-ad10-7bde63707c2d', 'majata@gmgmg.ssd'), 
('13e95981-9dcb-4bb9-9c9d-0c0b4b4682dd', '6e27f58a-f8f9-4b2c-b15b-d572a1c3b8f5', 'ivanmail@mail.ivan'), 
('97d63e11-4b76-49e3-8d23-a28b935e32d7', 'dcdb12a1-b2eb-454f-811b-59085ac2b4a6', 'lucz1234@gdsas.asss');


INSERT INTO Tags(Id, Tag)
VALUES ('619b941b-9d65-448e-9ad9-c949727260f4', 'fam'), 
('5d46bf95-fa7c-4436-87be-20118260799f', 'frend');

INSERT INTO ContactTag(ContactId, TagId)
VALUES ('3b4ce856-3126-4c29-ad10-7bde63707c2d', '619b941b-9d65-448e-9ad9-c949727260f4'), 
('3b4ce856-3126-4c29-ad10-7bde63707c2d', '5d46bf95-fa7c-4436-87be-20118260799f'), 
('50e4cf4e-6847-4686-b16b-2bae3374c74a', '5d46bf95-fa7c-4436-87be-20118260799f'), 
('6e27f58a-f8f9-4b2c-b15b-d572a1c3b8f5', '619b941b-9d65-448e-9ad9-c949727260f4'), 
('ff723f2e-7e5b-473d-b958-08b16ef0ba9b', '619b941b-9d65-448e-9ad9-c949727260f4');
