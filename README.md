# Contact list #

Application which supports adding, deleting and editing contacts. 
Each contact can have multiple numbers, emails and tags. First name of the contact is required while other fields are optional. Application also supports bookmarking and searching contacts. 

Browser bookmarks are supported as well as in-application bookmarks. To add phone numbers or emails, user must first opet contact details modal and enter new data there and to add new tags there is "Tags" button at home page from where user can edit, create or delete tags. Once in contact details, all tags will be listed and to add contact in certain tag, user must check the box next to it's name.


Frontend is made with AngularJS and Bootstrap. 

Backend (Web API) is made with ASP.NET Core (.NET Framework) and is published on Azure.

SQL Server is used as database and backend is connected to it via SqlConnection class.

### Database scheme: 
#### Contacts
 - Id - PK
 - FirstName - Required
 - LastName
 - Nickname
 - Address
 - IsBookmarked
 
#### PhoneNumbers
 - Id - PK
 - OwnerId - FK references Contacts(Id)
 - PhoneNumber - Required

#### Emails
 - Id - PK
 - OwnerId - FK references Contacts(Id)
 - Email - Required
 
#### Tags
 - Id - PK
 - Tag - Required
 
#### ContactTag
 - ContactId - FK references Contacts(Id)
 - TagId - FK references Tags(Id)
 - (ContactId, TagId) - PK

### Notes:

* Contact and search modals cannot be displayed on Chrome because of CORS, application was tested and works on Firefox